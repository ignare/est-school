import lightspeed as ls
import numpy as np

# ResourceList is a class encapsulating a set of CPU and/or GPU resources to
# use in computationally-intensive operations such as molecular integrals.

# => Build CPU+GPU ResourceList <= #

resources = ls.ResourceList.build()
print resources

# => Build CPU-only ResourceList <= #

resources = ls.ResourceList.build_cpu()
print resources
